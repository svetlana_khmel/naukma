const COLOR_BLACK_CLASS = 'black';
const COLOR_WHITE_CLASS = 'white';

//const horsePositions = [[2,2], [7,7], [5,5]];
//const horsePositions = [[9,2]];
//const horsePositions = [[10,1]];
const queenPositions = [[5,5]];
let horsePositions = [];
let horseAttackPositions = [];
let horseAttackPositionsFlat = [];

let queenAttackPositions = [];
let queenAttackPositionsFlat = [];
// Priority by index

function generateRandomIntegerInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomPositionGenerator(horsesValue) {
    const horsePositionsStrings = [];

    while (horsePositionsStrings.length < horsesValue) {
        let pair = []
        pair[0] = generateRandomIntegerInRange(1, 10);
        pair[1] = generateRandomIntegerInRange(1, 10);

        if (!horsePositionsStrings.includes(pair.toString())) {
            horsePositions.push(pair);
            horsePositionsStrings.push(pair.toString());
        }

        console.log('horsePositions ', horsePositions);
        console.log('horsePositionsStrings ', horsePositionsStrings);
    }
}

function calculateQueenAttackPositions(queenPosY, queenPosX, rowsValue, colsValue){
    let positions = [];

    for (let i = 1; i <= rowsValue; i++) {
        for (let j = 1; j <= colsValue; j++) {
            if (i === queenPosX && j === queenPosY) {
                continue;
            }
            if (i === queenPosX) {
                positions.push([i, j]);
            }
            if (j === queenPosY) {
                positions.push([i, j]);
            }

            if(Math.abs(queenPosX - i) === Math.abs(queenPosY - j)) {
                positions.push([i, j]);
            }
        }
    }

    return positions;
}

function getQueenAttackPositions(rowsValue, colsValue) {
    for(let k = 0; k< queenPositions.length; k++) {
        queenAttackPositions[k] = calculateQueenAttackPositions(queenPositions[k][0], queenPositions[k][1], rowsValue, colsValue);

        queenAttackPositionsFlat = queenAttackPositions.flat();
    }
}

function calculateHorseAttackPositions(i, j) {
    return [[i-2, j-1], [i-2, j+1], [i-1, j+2], [i+1, j+2], [i+2, j+1], [i+2, j-1], [i+1, j-2], [i-1, j-2]];
}

function getHorseAttackPositions() {
    for (let k = 1; k <= horsePositions.length; k++) {
        horseAttackPositions[k] = calculateHorseAttackPositions(horsePositions[k-1][0], horsePositions[k-1][1]);
        horseAttackPositionsFlat = horseAttackPositions.flat()
    }
}

function checkIfQueenAttack(i, j, rowsValue, colsValue) {
    getQueenAttackPositions(rowsValue, colsValue);

    const queenAttackPositions = queenAttackPositionsFlat;
    for (let r = 0; r <= queenAttackPositions.length; r++) {
        if (queenAttackPositions[r]) {
            if (queenAttackPositions[r][0] === i && queenAttackPositions[r][1] === j) {
                return queenAttackPositions[r][0] === i && queenAttackPositions[r][1] === j;
            }

        }
    }
}
function checkIfHorseAttack(i, j) {
    getHorseAttackPositions();
    const horseAttackPositions = horseAttackPositionsFlat;

    for (let r = 0; r <= horseAttackPositions.length; r++) {
        if (horseAttackPositions[r]) {
            if (horseAttackPositions[r][0] === i && horseAttackPositions[r][1] === j) {
                return horseAttackPositions[r][0] === i && horseAttackPositions[r][1] === j;
            }

        }
    }
}

function checkPlaceHorse (i,j) {
    for (let k = 0; k <= horsePositions.length; k++) {
        if(horsePositions[k] && horsePositions[k][0] === i && horsePositions[k][1] === j) {
            return true;
        }
    }
}

function checkPlaceQueen (i,j) {
    for (let k = 0; k <= queenPositions.length; k++) {
        if(queenPositions[k] && queenPositions[k][0] === i && queenPositions[k][1] === j) {
            return true;
        }
    }
}

function getCellColor(currentColor) {
    return currentColor === COLOR_BLACK_CLASS ? COLOR_WHITE_CLASS : COLOR_BLACK_CLASS
}

function changeStartColor (currentColor) {
    return currentColor === COLOR_BLACK_CLASS ? COLOR_WHITE_CLASS : COLOR_BLACK_CLASS
}

function drawChessBoard () {
    const boardEl = document.querySelector('.chess-board');
    const rows = document.getElementById('rows');
    const cols = document.getElementById('cols');
    const horses = document.getElementById('horses');

    const rowsValue = parseInt(rows.value);
    const colsValue = parseInt(cols.value);
    const horsesValue = parseInt(horses.value);

    randomPositionGenerator(horsesValue);

    // generateHorses(horsesValue, rowsValue, colsValue);

    let board = '';
    let rowEl = '';
    let prevStartColor = '';
    let currentColor = '';

    for (let i = 1; i <= rowsValue; i++) {
        let startColor = changeStartColor(prevStartColor || COLOR_WHITE_CLASS);
        currentColor = startColor;
        prevStartColor = startColor;

        for (let j = 1; j <= colsValue; j++) {
            currentColor = getCellColor(currentColor, j);

            const horseAttackCellClass = checkIfHorseAttack(i, j) ? 'place-horse-attack' : '';
            const queenAttackCellClass = checkIfQueenAttack(i, j, rowsValue, colsValue) ? 'place-queen-attack' : '';
            const placeHorseClass = checkPlaceHorse(i,j) ? 'place-horse' : '';
            const checkPlaceQueenClass = checkPlaceQueen(i,j) ? 'place-queen' : '';

            rowEl += `<div class="${currentColor} ${placeHorseClass} ${horseAttackCellClass}${checkPlaceQueenClass} ${queenAttackCellClass} cell"></div>`;
        }

        board += `<div class="row-cell">${rowEl}</div>`;
        rowEl = '';
    }

    boardEl.innerHTML = board;
}

function clearChessBoard() {
    horsePositions = [];
    horseAttackPositions = [];
    horseAttackPositionsFlat = [];
}

drawChessBoard();

window.addEventListener('DOMContentLoaded', (event) => {
    const button = document.getElementById('button');
    const clear = document.getElementById('clear');

    button.addEventListener('click', () => {
        drawChessBoard();
    });

    clear.addEventListener('click', () => {
        clearChessBoard();
    });


    console.log('DOM fully loaded and parsed');
});
