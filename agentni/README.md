
See screenshot:
https://res.cloudinary.com/dmt6v2tzo/image/upload/v1632986856/Screenshot_2021-09-30_at_10.27.03_rpe1od.png
----------------------
Лабораторна. Алгоритми пошуку і CSP.
----------------------

Застосування двох методів для розв’язання N - фігурної проблеми на дошці розміру MxK.

Завдання: розв’язати свій варіант методами ABT та DB
Зауваження. Найкраще робити для довільних фігур взагалі, тоді краще зрозумієте ідею методу.

Розв’язок задачі має бути імплементовано для будь-якої кількості тих і інших фігур разом або окремо.

Варіант 1

Фігури - ферзі і коні.

## Resources

Watch:

1. https://www.youtube.com/watch?v=j28zE3kPiNY
2. https://rstudio-pubs-static.s3.amazonaws.com/323738_75aa17495d9b4d6eb78266ba57b04d31.html
3. file:///Users/svetlanak/Downloads/The_message_management_asynchronous_backtracking_a.pdf
## Libs

1. JSXGraph https://jsxgraph.uni-bayreuth.de/
### nQueen problem backtracking:   
2. https://www.geeksforgeeks.org/n-queen-problem-backtracking-3/?ref=lbp
3. !! My code works from this site **** https://www.codesdope.com/course/algorithms-backtracking/
