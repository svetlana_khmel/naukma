const RED = 'red';
const BLUE = 'blue';
const GREEN = 'green';
const YELLOW = 'yellow';

const colors = [RED, BLUE, GREEN, YELLOW];
//const dotsPositions = [[2,2], [7,7], [5,5]];

let dotsPositions = [];
let linkedGraph = [];
let badLinks = 0;

// End of ABT

// розсилання повідомлень ок? зі своїм кольором сусідам з нижчим пріоритетом

function getElementsExceptCurrent(array, current) {
    console.log(current);
    let arr = [];
    for (let i = 0; i < array.length; i++) {
        if (i !== current) {
            arr.push(array[i]);
        }
    }
    return arr;
}

function handleAddNeighbour(){

}

function handleMessages() {
    linkedGraph.forEach(link => {
        if (link[2].messageQueue.length !== 0) {
            link[2].messageQueue.forEach(message => {
                if (message.type === 'ok') {
                    handleOk(message, link);
                } else if (message.type === 'nogood') {

                } else {
                    handleAddNeighbour(message);
                }
            });
        }
    });
}

function handleOk(message, link) {
    const {node, color, type } = message;

    link[2].localView.push({
        node, color
    })

    checkLocalView(link);
}
// перевірка сумісності поточного кольору nogood
function checkLocalView(link) {
    if (!canIBe(link)) {

        let tryColors = colors.filter(color => {   // remove our current color from the list
            return color !== link[2].agentView.color;
        });

        let i = 0;
        let canBeSomethingElse = false;
        while(tryColors.length !==0) { // check all colors
            link[2].localView.push({
                color: tryColors[i],
            });

            i++;

            if(canIBe(link)) {
                tryColors = [];
                canBeSomethingElse = true;
                sendOutNewValue(link);
            }

            if (tryColors.length !==0 && i === tryColors.length && canBeSomethingElse === false) {
                backtrack(link)
            }
        }
    }
}

function canIBe(link) {
    link[2].noGoods.forEach(item => {
        if (item.color === item[2].localView.color) {
            link[2].localView.pop();
            return false;
        } else {
            link[2].localView.pop(); // it was added just to check
            return true;
        }
    });
}

// Процедура відкату
function backtrack(link) {
    // викідуємо дублікааті з nogood
    normalizeNoGood();
    findNewNogood(link);
}

function findNewNogood(link) {
    // return current local-view
    return link[2].localView;
}

function ifViolates(assignments, constraint) {
//     foreach constraint [
//         [a] ->
//     if not (table:has-key? assignments (first a) and (table:get assignments first a) = (last a)) [report false]
// ]
//     report true

    //getBadLinks(link1, link2)
}


function toGo() {
    for(let i = 0; i <= linkedGraph.length; i++) {
        if (linkedGraph[2].messageQueue.length !== 0) {
            handleMessages(linkedGraph[2].messageQueue);
        } else if (badLinks === 0) {
            console.log('SOLUTION FOUND');
        } else {
            console.log('NO MORE MESSAGES, NO SOLUTION');
        }
    }
}

function getBadLinks(link1, link2) {
    return link1[2].agentView.color === link2[2].agentView.color //bad link, the same color
}

function ifConstrainViolated() {
    // should return true/false
}

function normalizeNoGood() {
    // Sorting restrictions for removing duplilcates
}

function sendOutNewValue(node) {
    // send ok? to lowerNeighbors
    // coordinate, color, ok?
    const message = {
        node: node[0],
        color: node[2].agentView.color,
        type:''
    }
    node[2].lowerNeighbors.forEach( neighbor => {
            findNeighbour(neighbor, message);
        }
    )
}

function findNeighbour(neighbor, message) {
    linkedGraph.forEach(link => {
        if (link[0].toString() === neighbor.toString()) {
            link[2].messageQueue.push(message)
        }
    });
}

function createLinkWithOneOfOtherNodes() {
    for (let i = 0; i < dotsPositions.length; i++) {
        const array = getElementsExceptCurrent(dotsPositions, i); // one to all
        const color = colors[Math.floor(Math.random() * 4)];

        let lowerNeighbors = [];
        let noGoods = [];

        dotsPositions[i]?.push(i); // set priority
        linkedGraph[i] = [dotsPositions[i], array, {
            agentView: {
                    color, // set random initial color
                },
                lowerNeighbors,
                noGoods: noGoods, // я та сусід маємо однаковий колір
                messageQueue: [],
                localView: [] //Сусіди - номер, колір
            }
        ];

        lowerNeighbors = linkedGraph.filter(el => {
            return el[0][2] > i;
        });

        console.log('----linkedGraph ', linkedGraph);
    }

    setNoGoods(linkedGraph);
}

function setNoGoods(linkedGraph) {
    linkedGraph.forEach(node => {
        node[1].forEach((neighbour, index) => {
            const nodePosition = node[1][index][2];

            if (linkedGraph[nodePosition][2].agentView.color === node[2].agentView.color) {
                console.log('linkedGraph[nodePosition][2].agentView.color.....', linkedGraph[nodePosition][2].agentView.color);
                console.log('node[2].agentView.color.... ', node[2].agentView.color);
                node[2].noGoods.push(linkedGraph[nodePosition]);

                const message = {
                    node: node[0],
                    color: node[2].agentView.color,
                    type:''
                }
                sendOutNewValue(message);
            }
        });
    });
    console.log('linkedGraph ', linkedGraph);
}

// End of ABT

function getRandomNumber(elementsNumber, florNumber) {
    const nums = new Set();
    while(nums.size !== 4) {
        nums.add(Math.floor(Math.random() * 4) + 1);
    }

    console.log([...nums]);
}

// function getDotsColor() {
//     //const indexed = getRandomNumber();
//     return colors[Math.floor(Math.random() * 4)]
// }

function generateRandomIntegerInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomPositionGenerator(dotsValue) {
    const dotsPositionsStrings = [];

    while (dotsPositionsStrings.length < dotsValue) {
        let pair = []
        pair[0] = generateRandomIntegerInRange(1, 10);
        pair[1] = generateRandomIntegerInRange(1, 10);

        if (!dotsPositionsStrings.includes(pair.toString())) {
            dotsPositions.push(pair);
            dotsPositionsStrings.push(pair.toString());
        }
    }
}

function checkPlaceDots (i,j) {
    for (let k = 0; k <= dotsPositions.length; k++) {
        if(dotsPositions[k] && dotsPositions[k][0] === i && dotsPositions[k][1] === j) {
            return true;
        }
    }
}

function checkColor (i,j) {
    for (let k = 0; k <= linkedGraph.length; k++) {
        if(linkedGraph[k] && linkedGraph[k][0][0] === i && linkedGraph[k][0][1] === j) {
            return linkedGraph[k][2].agentView.color;

        }
    }
}

function drawChessBoard () {
    const boardEl = document.querySelector('.chess-board');
    const rows = document.getElementById('rows');
    const cols = document.getElementById('cols');
    const dots = document.getElementById('dots');

    const rowsValue = parseInt(rows.value);
    const colsValue = parseInt(cols.value);
    const dotsValue = parseInt(dots.value);

    randomPositionGenerator(dotsValue);
    createLinkWithOneOfOtherNodes();

    let board = '';
    let rowEl = '';

    for (let i = 1; i <= rowsValue; i++) {
        for (let j = 1; j <= colsValue; j++) {
            const dotColor = checkColor(i,j);
            const placeDotsClass = checkPlaceDots(i,j) ? dotColor : '';

            rowEl += `<div class=" ${placeDotsClass} cell"></div>`;
        }

        board += `<div class="row-cell">${rowEl}</div>`;
        rowEl = '';
    }

    boardEl.innerHTML = board;
}

function clearChessBoard() {
    dotsPositions = [];
}

drawChessBoard();

window.addEventListener('DOMContentLoaded', (event) => {
    const button = document.getElementById('button');
    const clear = document.getElementById('clear');
    const go = document.getElementById('go');

    button.addEventListener('click', () => {
        drawChessBoard();
    });

    clear.addEventListener('click', () => {
        clearChessBoard();
    });

    go.addEventListener('click', () => {
        toGo();
    })

    console.log('DOM fully loaded and parsed');
});

