class Backtracking {
    isAttack(i, j, board, N) {
        let k, l;
        // checking for column j
        for (k = 1; k <= i - 1; k++) {
            if (board[k][j] == 1)
                return true;
        }

        // checking upper right diagonal
        k = i - 1;
        l = j + 1;
        while (k >= 1 && l <= N) {
            if (board[k][l] == 1)
                return true;
            k = k + 1;
            l = l + 1;
        }

        // checking upper left diagonal
        k = i - 1;
        l = j - 1;
        while (k >= 1 && l >= 1) {
            if (board[k][l] == 1)
                return true;
            k = k - 1;
            l = l - 1;
        }

        return false;
    }

    nQueen(row, n, N, board) {
        if (n == 0) {
            console.log(board);
            return true;
        }

        for (let j = 1; j <= N; j++) {
            if (!this.isAttack(row, j, board, N)) {
                board[row][j] = 1;

                if (this.nQueen(row + 1, n - 1, N, board))
                    return true;

                board[row][j] = 0; // backtracking
            }
        }
        return false;
    }

    main() {
        const BOARD_SIZE = 5;
        const QUEEN_NUMBER = 4;

        let board = Array.from({length: BOARD_SIZE}, (v, k) => [...Array.from({length: BOARD_SIZE}, (v, k) => 0)]);
        this.nQueen(1, QUEEN_NUMBER, 4, board);
    }
}

const backtracking = new Backtracking();
backtracking.main();
