const UNDER_ATTACK = true;
const INITIAL_ROW = 1;

const COLOR_BLACK_CLASS = 'black';
const COLOR_WHITE_CLASS = 'white';

class Backtracking {
    inBoardArea(coordinate, boardSize) {
        if (coordinate >= 0 && coordinate < boardSize) return true;
    }

    checkPosition(x, y, boardSize) {
        return this.inBoardArea(x, boardSize) && this.inBoardArea(y, boardSize);
    }

    isQueenAttacks(k, l, i, j, board) {
        for (k = 1; k <= board.length; k++) {
            for (let l = 1; l <= board.length; l++) {
                const onBoard = this.checkPosition(k, l, board.length)
                if (k === i && l === j) {
                    continue;
                }
                if (onBoard && k === i && board[k][l] === 1 || onBoard && board[k][l] === 2) {
                    return UNDER_ATTACK;
                }
                if (l === j && onBoard && board[k][l] === 1 || onBoard && board[k][l] === 2) {
                    return UNDER_ATTACK;
                }

                if (Math.abs(i - k) === Math.abs(j - l) && onBoard && board[k][l] === 1 || onBoard && board[k][l] === 2) {
                    return UNDER_ATTACK;
                }
            }
        }
    }

    isAttack(i, j, board, N, subject) {
        let k, l;
        if (subject === 'queen') {
            if (this.isQueenAttacks(k, l, i, j, board)) {
                return UNDER_ATTACK;
            }
        } else {
            const horsePositions = [
                [-2, -1],
                [-2, +1],
                [-1, +2],
                [+1, +2],
                [+2, +1],
                [+2, -1],
                [+1, -2],
                [-1, -2]
            ];

            horsePositions.map((position, index) => {
                const onBoard = this.checkPosition(position[0], position[1], board.length);

                if (onBoard && board[position[0], position[1]] === 1 & board[position[0], position[1]] === 2) {
                    return UNDER_ATTACK;
                }
            })

            if (this.isQueenAttacks(k, l, i, j, board)) {
                return UNDER_ATTACK;
            }
        }

        return subject === 'queen' ? 1 : 2;
    }

    nQueen(row, queenNumber, horseNumber, boardSize, board) {
        if (queenNumber == 0) {
            console.log(board);
            return true;
        }

        for (let j = 1; j < boardSize; j++) {
            if (this.isAttack(row, j, board, boardSize, 'queen') === 1) {
                board[row][j] = 1;

                if (this.nQueen(row + 1, queenNumber - 1, horseNumber, boardSize, board))
                    return true;

                // backtracking
                board[row][j] = 0;
            }
        }
        return false;
    }

    nHorses(row, queenNumber, horseNumber, boardSize, board) {
        if (horseNumber == 0) {
            return true;
        }

        for (let j = 1; j < this.BOARD_SIZE; j++) {
            if (this.isAttack(row, j, board, boardSize, 'horse') === 2) {
                board[row][j] = 2;
                horseNumber -= 1;

                if (this.nHorses(row + 1, queenNumber, horseNumber, boardSize, board)) {
                    return true;
                }

                // backtracking
                if (board[row][j] !== 1) {
                    board[row][j] = 0;
                }
            }
        }
        return false;
    }

    main() {
        const BOARD_SIZE = 5;
        const QUEEN_NUMBER = 4;
        const HORSES_NUMBER = 1;

        let board = Array.from({length: BOARD_SIZE}, (v, k) => [...Array.from({length: BOARD_SIZE}, (v, k) => 0)]);
        this.nQueen(INITIAL_ROW, QUEEN_NUMBER, HORSES_NUMBER, BOARD_SIZE, board);
        this.nHorses(INITIAL_ROW, 0, HORSES_NUMBER, BOARD_SIZE, board);
    }
}

const backtracking = new Backtracking();
backtracking.main();
