const QUEEN = 1;
const HORSE = 2
const UNDER_ATTACK = true;

class DB {
    inBoardArea(coordinate) {
        if (coordinate >= 0 && coordinate < this.BOARD_SIZE) return true;
    }

    checkPosition(x, y) {
        return this.inBoardArea(x) && this.inBoardArea(y);
    }

    createInitialPositions() {
        for (let i = 0; i<this.BOARD_SIZE; i++) {
            this.INITIAL_POSITIONS.push([this.posX[i],this.posY[i] ]);
        }

        console.log('this.INITIAL_POSITIONS  ', this.INITIAL_POSITIONS);
        console.log('this.posX  ', this.posX);
        console.log('this.posY  ', this.posY);
    }

    generateRandomArray() {
        const nums = new Set();
        const figuresNumber = this.figures.length;
        while (nums.size !== figuresNumber) {
            nums.add(Math.floor(Math.random() * figuresNumber) + 1);
        }

        return [...nums];
    }


    findProblemByFigurePosition() {

    }

    printBoard() {
        let board = '';
        let boardToStore = [];

        this.placeFigureToBoard();

        for (let i = 0; i < this.BOARD_SIZE; i++) {
            board += this.board[i].join('|')+ '\n';
        }


        console.log('.....',this.board);
        console.log(board);
    }

    isQueenAttacks(row, j,figureNumber,goal) {
        let result = 0;

        for (let k = 0; k < this.board.length; k++) {
            for (let l = 0; l < this.board[0].length; l++) {
                const onBoard = this.checkPosition(k, l);

                if (this.board[k][l] != figureNumber && onBoard && !isNaN( this.board[k][l])) {
                    if (onBoard && k === row ) {
                        if(goal === 1) {
                            this.conflictWeight[figureNumber][this.board[k][l]] +=1;  //weight of conflict between figureNumber and board[k][l]
                            result +=this.conflictWeight[figureNumber][this.board[k][l]] ;
                        } else {
                            result += this.conflictWeight[figureNumber][this.board[k][l]] + 1;
                        }

                    }
                    if (l === j ) {
                        if(goal === 1) {
                            this.conflictWeight[figureNumber][this.board[k][l]] +=1;  //weight of conflict between figureNumber and board[k][l]
                            result +=this.conflictWeight[figureNumber][this.board[k][l]] ;
                        } else {
                            result += this.conflictWeight[figureNumber][this.board[k][l]] + 1;
                        }
                    }

                    if (Math.abs(row - k) === Math.abs(j - l )) {
                        if(goal === 1) {
                            this.conflictWeight[figureNumber][this.board[k][l]] +=1;  //weight of conflict between figureNumber and board[k][l]
                            result +=this.conflictWeight[figureNumber][this.board[k][l]] ;
                        } else {
                            result += this.conflictWeight[figureNumber][this.board[k][l]] + 1;
                        }
                    }
                }

            }
        }

        const horsePositions = [
            [-2, -1],
            [-2, +1],
            [-1, +2],
            [+1, +2],
            [+2, +1],
            [+2, -1],
            [+1, -2],
            [-1, -2]
        ];

        for (let i = 0; i < horsePositions.length; i++) {
            const onBoard = this.checkPosition(row + horsePositions[i][0], j + horsePositions[i][1], this.board.length);
            const posX = parseInt(row)  + parseInt(horsePositions[i][0]);
            const posY = parseInt(j)  + parseInt(horsePositions[i][1]);

            if (this.board[posX] && this.board[posX][posY]) {
                if (onBoard && this.board[posX][posY] >= this.countQueens) { //queens is betten by horses
                    this.conflictWeight[figureNumber][this.board[posX][posY]] +=1  //weight of conflict between figureNumber and board[k][l]

                    result +=this.conflictWeight[figureNumber][this.board[posX][posY]] ;
                }
            }
        }

        return result ;
    }


    placeFigureToBoard() {
        let placed = 0;

        while (placed < this.INITIAL_POSITIONS.length-2) {
            if (this.figures[placed]) {
                this.board[this.INITIAL_POSITIONS[placed][0]][this.INITIAL_POSITIONS[placed][1]] = placed;
                console.log('board ', this.board);
                console.log('placed ', placed);
                console.log('this.figures[placed] ', this.figures[placed]);
            }
            placed++;
        }
    }

    isHorseAttack(row, j, figureNumber) {
        let result = 0;

        // if (this.board[row][j] === 2) {

        //  result +=1;
        //}
        for (let k = 0; k < this.board.length; k++) {
            for (let l = 0; l < this.board[0].length; l++) {
                const onBoard = this.checkPosition(k, l);

                if (this.board[k][l] != figureNumber && onBoard &&
                    !isNaN(this.board[k][l]) && this.board[k][l] < this.countQueens) {
                    if (onBoard && k === row) {
                        this.conflictWeight[figureNumber][this.board[k][l]] += 1  //weight of conflict between figureNumber and board[k][l]

                        result += this.conflictWeight[figureNumber][this.board[k][l]];
                    }
                    if (l === j) {
                        this.conflictWeight[figureNumber][this.board[k][l]] += 1  //weight of conflict between figureNumber and board[k][l]

                        result += this.conflictWeight[figureNumber][this.board[k][l]];
                    }

                    if (Math.abs(row - k) === Math.abs(j - l)) {
                        this.conflictWeight[figureNumber][this.board[k][l]] += 1  //weight of conflict between figureNumber and board[k][l]

                        result += this.conflictWeight[figureNumber][this.board[k][l]];
                    }

                }
            }
        }

        const horsePositions = [
            [-2, -1],
            [-2, +1],
            [-1, +2],
            [+1, +2],
            [+2, +1],
            [+2, -1],
            [+1, -2],
            [-1, -2]
        ];

        for (let i = 0; i < horsePositions.length; i++) {
            const onBoard = this.checkPosition(row + horsePositions[i][0], j + horsePositions[i][1], this.board.length);
            const posX = parseInt(row)  + parseInt(horsePositions[i][0]);
            const posY = parseInt(j)  + parseInt(horsePositions[i][1]);

            if (this.board[posX] && this.board[posX][posY]) {
                if (onBoard && !isNaN(this.board[posX][posY])) {
                    this.conflictWeight[figureNumber][this.board[posX][posY]] +=1  //weight of conflict between figureNumber and board[k][l]

                    result +=this.conflictWeight[figureNumber][this.board[posX][posY]] ;
                }
            }
        }

        return result;
    }

    checkConflicts(x,y,figureNumber) {
        //for (let k = 0; k< this.figures.length; k++) {
        //console.log('checking gueens  ... ');
        if (this.figures[figureNumber] === QUEEN) {
            return this.isQueenAttacks(x, y,figureNumber);
        }
        if (this.figures[figureNumber] === HORSE) {
            return this.isHorseAttack(x, y, figureNumber)
        }
        // }
    }


    resolveConflict(figureNumber) {
        let kXPoss = this.INITIAL_POSITIONS[figureNumber][0];
        let kYPoss = this.INITIAL_POSITIONS[figureNumber][1];
        //let number = this.figures[figureNumber];
        let BestXPoss = kXPoss;
        let BestYPoss = kYPoss;
        let BestQualityProblems = this.figuresHasConflicts[figureNumber];
        this.board[kXPoss][kYPoss] = '_';

        for (let i = 0; i < this.board.length; i++) {
            for (let j = 0; j < this.board[0].length; j++) {
                if (isNaN(this.board[i][j])) {
                    this.board[i][j] = figureNumber;
                    if(this.checkConflicts(i,j,figureNumber,2) == 0) { // best resolve of the problem (0), best position [i, j] , 2-test
                        BestQualityProblems = this.figuresHasConflicts[figureNumber];
                        BestXPoss = i;
                        BestYPoss = j;
                        this.board[i][j] = '_';
                        this.board[kXPoss][kYPoss] =figureNumber ;
                        return [BestQualityProblems, BestXPoss, BestYPoss];
                    };
                    if(this.checkConflicts(i,j,figureNumber,2) < BestQualityProblems) {
                        BestQualityProblems = this.figuresHasConflicts[figureNumber]- this.checkConflicts(i,j,figureNumber,2);
                        BestXPoss = i;
                        BestYPoss = j;
                        this.board[i][j] = '_';
                    }
                }
            }
        }
        this.board[kXPoss][kYPoss] =figureNumber ;
        return [BestQualityProblems, BestXPoss, BestYPoss];
    }


    main() {
        this.figures = [QUEEN, QUEEN, QUEEN, HORSE, HORSE, HORSE];
        this.countQueens = 3;
        this.countHorses = 3;
        this.figuresHasConflicts = [0,0,0,0,0,0];
        this.BOARD_SIZE = 10;
        this.INITIAL_POSITIONS = [];
        this.conflictWeight = [[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]];


        this.posX = this.generateRandomArray();
        this.posY = this.generateRandomArray();

        this.board = Array.from({length: this.BOARD_SIZE}, (v, k) => [...Array.from({length: this.BOARD_SIZE}, (v, k) => '_')]);

        this.createInitialPositions();
        this.printBoard();
        ///this.checkConflicts();
        for (let i = 0; i < this.figures.length; i++) {
            this.figuresHasConflicts[i] = this.checkConflicts(this.INITIAL_POSITIONS[i][0], this.INITIAL_POSITIONS[i][1], i);

        }

        this.printBoard();
        console.log('this.conflictWeight >>>> :', this.conflictWeight);
        console.log('this.figuresHasConflicts >>>> :', this.figuresHasConflicts);

        console.log('this.resolveConflict(1) 1: ', this.resolveConflict(1)[0]);
        console.log('this.resolveConflict(1) 1: ', this.resolveConflict(1)[1]);
        console.log('this.resolveConflict(1) 1: ', this.resolveConflict(1)[2]);

    }
}

const db = new DB();
db.main();



