const UNDER_ATTACK = true;
const UNDER_QUEEN_ATTACK = '*';
let queensLastIteration = false;

class Backtracking {
    inBoardArea(coordinate) {
        if (coordinate >= 0 && coordinate < this.BOARD_SIZE) return true;
    }

    checkPosition(x, y) {
        return this.inBoardArea(x) && this.inBoardArea(y);
    }

    isQueenAttacks(row, j) { //
        for (let k = 1; k <= this.board.length; k++) {
            for (let l = 1; l <= this.board[0].length; l++) {
                const onBoard = this.checkPosition(k, l);

                if (onBoard && k === row && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
                if (l === j && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }

                if (Math.abs(row - k) === Math.abs(j - l) && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
            }
        }

        const horsePositions = [
            [-2, -1],
            [-2, +1],
            [-1, +2],
            [+1, +2],
            [+2, +1],
            [+2, -1],
            [+1, -2],
            [-1, -2]
        ];

        for (let i = 0; i < horsePositions.length; i++) {
            const onBoard = this.checkPosition(row + horsePositions[i][0], j + horsePositions[i][1], this.board.length);
            const posX = parseInt(row)  + parseInt(horsePositions[i][0]);
            const posY = parseInt(j)  + parseInt(horsePositions[i][1]);

            if (this.board[posX] && this.board[posX][posY]) {
                if (onBoard && this.board[posX][posY] === 2) {
                    return UNDER_ATTACK;
                }
            }
        }
    }

    isHorseAttack(row, j) {
        if (this.board[row][j] === 2) {
            return UNDER_ATTACK;
        }
        for (let k = 1; k <= this.board.length; k++) {
            for (let l = 1; l <= this.board[0].length; l++) {
                const onBoard = this.checkPosition(k, l);

                if (onBoard && k === row && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
                if (l === j && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }

                if (Math.abs(row - k) === Math.abs(j - l) && onBoard && this.board[k][l] === 1) {
                    return UNDER_ATTACK;
                }
            }
        }

        const horsePositions = [
            [-2, -1],
            [-2, +1],
            [-1, +2],
            [+1, +2],
            [+2, +1],
            [+2, -1],
            [+1, -2],
            [-1, -2]
        ];

        for (let i = 0; i < horsePositions.length; i++) {
            const onBoard = this.checkPosition(row + horsePositions[i][0], j + horsePositions[i][1], this.board.length);
            const posX = parseInt(row)  + parseInt(horsePositions[i][0]);
            const posY = parseInt(j)  + parseInt(horsePositions[i][1]);

            if (this.board[posX] && this.board[posX][posY]) {
                if (
                    onBoard && this.board[posX][posY] === 1 ||
                    onBoard && this.board[posX][posY] === 2
                ) {
                    return UNDER_ATTACK;
                }
            }
        }

        return false;
    }

    findSolution(elementPosition) {
        if (elementPosition === this.entities.length) {
            this.printBoard();
            return true;
        } else if (elementPosition < this.entities.length) {
            if(this.entities[elementPosition] === 'Q') {
                for (let i = 1; i < this.board.length; i++) {
                    for (let j = 1; j < this.board[0].length; j++) {
                       if(!this.isQueenAttacks(i, j)) {
                           this.board[i][j] = 1;

                          if (this.findSolution(elementPosition + 1)) {
                                return true;

                          } else {
                              this.board[i][j] = 0;
                          }
                       }
                    }
                }
            }

            if (this.entities[elementPosition] === 'H') {
                        for (let i = 1; i < this.board.length; i++) {
                                for (let j = 1; j < this.board[0].length; j++) {
                                    if(!this.isHorseAttack(i, j)) {
                                        this.board[i][j] = 2;
                                        console.log('i ', i);
                                        console.log('j ', j);

                                        if (this.findSolution(elementPosition + 1)) {
                                            return true;

                                        } else {
                                            this.board[i][j] = 0;
                                        }
                                    }
                                }
                        }
            }
        }
        return false;
    }

    printBoard() {
        let board = '';
        for (let i = 0; i < this.BOARD_SIZE; i++) {
            board += this.board[i].toString() + '\n';
        }
        console.log(board);
    }

    main() {
        this.BOARD_SIZE = 10;
        const QUEEN_NUMBER = 4;
        const HORSE_NUMBER = 6;

        this.entities = [];

        for (let i = 0; i<QUEEN_NUMBER; i++ ){
            this.entities.push('Q');
        }

        for (let i = 0; i<HORSE_NUMBER; i++ ){
            this.entities.push('H');
        }

        console.log('this.entities  ', this.entities);

        this.board = Array.from({length: this.BOARD_SIZE}, (v, k) => [...Array.from({length: this.BOARD_SIZE}, (v, k) => 0)]);
        this.findSolution(0);
    }
}

const backtracking = new Backtracking();
backtracking.main();
