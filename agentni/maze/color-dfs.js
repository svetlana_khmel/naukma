const colors = ['red', 'blue', 'green'];// Доступні кольори

// кольори на початок
const nodes = {
    A: 'red',
    B: 'blue',
    C: 'blue',
    D: 'red',
    E: 'blue',
    F: 'blue',
}

class Queue {
    constructor() {
        this.items = [];
    }

    enqueue(element) {
        this.items.push(element);
    }

    dequeue() {
        if (this.isEmpty())
            return "Underflow";
        return this.items.shift();
    }

    front() {
        if (this.isEmpty())
            return "No elements in Queue";
        return this.items[0];
    }

    isEmpty() {
        return this.items.length == 0;
    }

    printQueue() {
        var str = "";
        for (var i = 0; i < this.items.length; i++)
            str += this.items[i] + " ";
        return str;
    }
}

class Graph {
    constructor(noOfVertices) {
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    addVertex(v) {
        this.AdjList.set(v, []);
    }

    printGraph() {
        var get_keys = this.AdjList.keys();
        for (var i of get_keys) {
            var get_values = this.AdjList.get(i);
            var conc = "";

            for (var j of get_values)
                conc += j + " ";

            console.log(i + " -> " + conc);
        }
    }

    addEdge(v, w) {
        this.AdjList.get(v).push(w);
        this.AdjList.get(w).push(v);
    }

    dfs(startingNode) {
        var visited = {};
        this.DFSUtil(startingNode, visited);
    }

    colorProblem(startingNode) {

        this.needToCHeck = true;
        while (this.needToCHeck) {
            this.dfs(startingNode)
        }
    }

// Обход нод
    DFSUtil(vert, visited) {
        visited[vert] = true;
        console.log(vert);

        var get_neighbours = this.AdjList.get(vert);

        for (var i in get_neighbours) {
            var get_elem = get_neighbours[i];
            if (!this.constraintViolations(vert, get_elem)) {
                this.changeColors(vert, get_elem);
            }
            console.log('nodes: ', nodes)
            if (!visited[get_elem])
                this.DFSUtil(get_elem, visited);
        }
    }

    changeColors(vert, get_elem) {
        let goodColor = false;
        while (goodColor === false) {
            colors.forEach(color => {
                nodes[get_elem] = color;
                goodColor = this.constraintViolations(vert, get_elem)
            })
        }
    }

    constraintViolations(nodeA, nodeB) {
        return nodes[nodeA] !== nodes[nodeB];
    }
}

// Using the above implemented graph class
var g = new Graph(6);
var vertices = ['A', 'B', 'C', 'D', 'E', 'F'];

// adding vertices
for (var i = 0; i < vertices.length; i++) {
    g.addVertex(vertices[i]);
}

// adding edges
g.addEdge('A', 'B');
g.addEdge('A', 'D');
g.addEdge('A', 'E');
g.addEdge('B', 'C');
g.addEdge('D', 'E');
g.addEdge('E', 'F');
g.addEdge('E', 'C');
g.addEdge('C', 'F');

g.printGraph();


console.log("DFS");
g.dfs('A');

