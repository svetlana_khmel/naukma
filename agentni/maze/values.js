SMALL_ENOUGH = 0.005
GAMMA = 0.9
NOISE = 0.10
ENV_ROWS = 5
ENV_COLUMNS = 5


//Define all states
let all_states = []



function chooseMaxValueinArray(array) {
    return Math.max(...array)

}

for (let i = 0; i< ENV_ROWS; i++) {
    all_states[i] = [];
    for (let j = 0; j< ENV_ROWS; j++) {
        all_states[i][j] = [i,j];
    }
}

let rewards = []

for (let i = 0; i< ENV_ROWS; i++) {
    rewards[i] = [];
    for (let j = 0; j< ENV_ROWS; j++) {
        rewards[i][j] = 0;
    }
}

rewards[1][2] = -1 //small reward
rewards[0][3] = -100 //fire hall
rewards[1][0] = -100 //fire hall
rewards[4][0] = -100 //fire hall
rewards[4][1] = -100 //fire hall
rewards[0][4] = -100 //fire hall
rewards[1][4] = -100 //fire hall
rewards[2][2] = -1 //small reward
rewards[4][4] = 100 //exit

let actions = ['up', 'right', 'down', 'left'];

let policy = []

for (let i = 0; i< ENV_ROWS; i++) {
    policy[i] = []
    for (let j = 0; j< ENV_ROWS; j++) {
        policy[i][j] = actions[Math.floor(Math.random() * 4)];
    }
}

let Values = []

for (let i = 0; i< ENV_ROWS; i++) {
    Values[i] = [];
    for (let j = 0; j < ENV_ROWS; j++) {
        Values[i][j] = 0;
    }
}



Values[1][2] = -1
Values[0][3] = -100
Values[1][0] = -100
Values[4][0] = -100
Values[4][1] = -100
Values[0][4] = -100
Values[1][4] = -100
Values[2][2] = -1
Values[4][4] = 100
console.log('Values ... ', Values)
iteration = 0

let training = true
while(training){
    let biggest_change = 0;
    let nextMove = []
    let nextAction = []
    for (let i = 0; i< all_states.length; i++) {
        for (let j = 0; j < all_states.length; j++) {
            let old_value = Values[i][j];
            let new_value = 0;
            console.log('old_value ', old_value)

            if(policy[i][j] === 'up') {
                nextMove[0] = all_states[i][j][0] -1;
                nextMove[1] = all_states[i][j][1];
            } else if (policy[i][j] === 'right') {
                nextMove[0] = all_states[i][j][0];
                nextMove[1] = all_states[i][j][1]+1;
            } else if (policy[i][j] === 'down') {
                nextMove[0] = all_states[i][j][0] +1;
                nextMove[1] = all_states[i][j][1];
            } else if (policy[i][j] === 'left') {
                nextMove[0] = all_states[i][j][0] -1;
                nextMove[1] = all_states[i][j][1];
            }

            // Choose a new random action to do (transition probability)
            let randomAction = actions[Math.floor(Math.random() * 4)];

            if ( randomAction === 'up') {
                nextAction[0] = all_states[i][j][0] -1;
                nextAction[1] = all_states[i][j][1];
            } else if (randomAction === 'right') {
                nextAction[0] = all_states[i][j][0];
                nextAction[1] = all_states[i][j][1]+1;
            } else if (randomAction === 'down') {
                nextAction[0] = all_states[i][j][0] +1;
                nextAction[1] = all_states[i][j][1];
            } else if (randomAction === 'left') {
                nextAction[0] = all_states[i][j][0] -1;
                nextAction[1] = all_states[i][j][1];
            }

            // Calculate the value
            if (Values[nextMove[0]] && Values[nextMove[0]][nextMove[1]]) {
                let calculated_value = rewards[i][j] + (GAMMA * ((1 - NOISE) * Values[nextMove[0]][nextMove[1]] + (NOISE * Values[nextAction[0]][nextAction[1]])))


                if (calculated_value > new_value) { //# Is this the best action so far? If so, keep it
                    new_value = calculated_value;
                    policy[i][j] = policy[i][j];
                }

                Values[i][j] = new_value;
                //biggest_change =  max(biggest_change, np.abs(old_v - V[s]))
                console.log('biggest_change ', biggest_change);
                console.log('Math.abs(old_value - Values[i][j]) ', Math.abs(old_value - Values[i][j]));
                biggest_change = chooseMaxValueinArray([biggest_change, Math.abs(old_value - Values[i][j])])
            }

        }


        // See if the loop should stop now
        if (biggest_change < SMALL_ENOUGH) {
            console.log('VVVV ', V)
            break
            iteration += 1
        }
    }
}





