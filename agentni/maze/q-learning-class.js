
class Q {
    generateRandomArray(size) {
        const nums = new Set();
        const figuresNumber = size;
        while (nums.size !== figuresNumber) {
            console.log('while generateRandomArray');
            nums.add(Math.floor(Math.random() * figuresNumber) + 1);
        }

        return [...nums];
    }

    createInitialPositions(size) {
        const positions = [];
        for (let i = 0; i< size; i++) {
            positions.push([this.posX[i],this.posY[i]]);
        }
    }

    printMaze(maze) {
        let board = '';

        for (let i = 0; i < maze.length; i++) {
            board += maze[i].join('|')+ '\n';
        }

        console.log(board.toString());
    }

    // define a function that determines if the specified location is a terminal state

    isTerminalState (currentRowIndex, currentColumnIndex) {
        if(this.rewards[currentRowIndex][currentColumnIndex] === -1) {
            return false;
        } else {
            return true;
        }
    }

    // define a function that will choose a random, non-terminal starting location
    getStartingLocation () {
        //get a random row and column index
        let currentRowIndex = Math.floor(Math.random() * this.environment_size);
        let currentColumnIndex = Math.floor(Math.random() * this.environment_size);

        // if (this.isTerminalState(currentRowIndex, currentColumnIndex)) {
        //     return [currentRowIndex, currentColumnIndex];
        // }

        while(this.isTerminalState(currentRowIndex, currentColumnIndex)) {
            console.log('while this.isTerminalState');
            currentRowIndex = Math.floor(Math.random() * this.environment_size);
            currentColumnIndex = Math.floor(Math.random() * this.environment_size);
        }
        return [currentRowIndex, currentColumnIndex];

        // continue choosing random row and column indexes until a non-terminal state is identified
        //(i.e., until the chosen state is a 'white square')
    }

    //define an epsilon greedy algorithm that will choose which action to take next (i.e., where to move next)
    getNextAction (currentRowIndex, currentColumnIndex, epsilon) {
        // if a randomly chosen value between 0 and 1 is less than epsilon,
        // then choose the most promising value from the Q-table for this state.
        return Math.floor(Math.random() * 4);
        if (this.chooseRandomAction() < epsilon) {
            return Math.max(...this.qValues[currentRowIndex, currentColumnIndex]);
        } else {
            //choose a random action
            return Math.floor(Math.random() * 4);
        }
    }

    //define a function that will get the next location based on the chosen action
    getNextLocation(currentRowIndex, currentColumnIndex, actionIndex) {
        let newRowIndex = currentRowIndex;
        let newColumnIndex = currentColumnIndex;

        if (this.actions[actionIndex] === 'up' && currentRowIndex > 0) {
            currentRowIndex -= 1;
        } else if (this.actions[actionIndex] === 'right' && currentColumnIndex < this.environmentColumns - 1) {
            newColumnIndex +=1;
        } else if (this.actions[actionIndex] === 'down' && this.currentRowIndex < this.environmentRows - 1) {
            newRowIndex -= 1;
        } else if (this.actions[actionIndex] === 'left' && currentColumnIndex > 0) {
            newColumnIndex -= 1;
        }

        return [newRowIndex, newColumnIndex];
    }
    // Define a function that will get the shortest path between any location within the warehouse that
    // the robot is allowed to travel and the item packaging location.

    getShortestPath (startRowIndex, startColumnIndex) {
        //return immediately if this is an invalid starting location
        if(this.isTerminalState(startRowIndex, startColumnIndex)){
            return [];
        } else {
            // if this is a 'legal' starting location
            let currentRowIndex = startRowIndex;
            let currentColumnIndex = startColumnIndex;
            let shortestPath = [];

            shortestPath.push([currentRowIndex, currentColumnIndex]);
            // continue moving along the path until we reach the goal (i.e., the item packaging location)
            // while (!this.isTerminalState(currentRowIndex, currentColumnIndex)) {
            // get the best action to take
            let actionIndex = this.getNextAction(currentRowIndex, currentColumnIndex, 1);
            // move to the next location on the path, and add the new location to the list

            // let [currentRowIndex, currentColumnIndex] = this.getNextLocation(currentRowIndex, currentColumnIndex, actionIndex);
            currentRowIndex = this.getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[0];
            currentColumnIndex = this.getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[1];
            shortestPath.push([currentRowIndex, currentColumnIndex]);
            //return shortestPath;
            // }


            // get the best action to take
            //let actionIndex = this.getNextAction(currentRowIndex, currentColumnIndex, 1);

            // move to the next location on the path, and add the new location to the list
            //[currentRowIndex, currentColumnIndex] = this.getNextLocation(currentRowIndex, currentColumnIndex, actionIndex);
            //shortestPath.append([currentRowIndex, currentColumnIndex])
            return shortestPath;

        }
    }


    chooseRandomAction() {
        return Math.random() < 0.5 ? 0 : 1;
    }

    // define a function that will choose a random, non-terminal starting location
    main() {
        this.environment_size = 5;
        // Старт
        // Пусті клітіни
        // Вогняні ями (при потраплянні у них гра закінчується)
        // Зелені клітинки “відпочинку” які дають невеликий бонус  (менше ніж винагорода за вихід).
        // Вихід

        this.actions = ['up', 'right', 'down', 'left'];

        const startPosition = [0,1];
        const endPosition = [0,1];

        this.fireHolsValue = -100;
        //this.fireHolsSize = this.environment_size/ 2;
        this.greenCellsValue = 10;
        //this.greenCellsSize = this.environment_size / 2;

        this.qValues = Array.from({
            length: this.environment_size
        }, (v, k) => [...Array.from({
            length: this.environment_size
        }, (v, k) => [0,0,0,0])]);


        this.rewards = Array.from({
            length: this.environment_size
        }, (v, k) => [...Array.from({
            length: this.environment_size
        }, (v, k) => -1)]);

        // this.maze[0][0] = 'S'; // start
        // this.maze[this.maze.length-1][this.maze.length-1] = 'X'; //exit

        this.rewards[0][5] = this.fireHolsValue;
        this.rewards[2][0] = this.fireHolsValue;
        this.rewards[2][3] = this.fireHolsValue;
        this.rewards[2][4] = this.fireHolsValue;
        this.rewards[3][0] = this.fireHolsValue;
        this.rewards[4][5] = this.fireHolsValue;

        this.rewards[1][3] = this.greenCellsValue;
        this.rewards[2][4] = this.greenCellsValue;
        this.rewards[3][5] = this.greenCellsValue;
        this.rewards[0][2] = this.greenCellsValue;
        this.rewards[4][3] = this.greenCellsValue;
        // this.rewards[4][4] = 100;



// Create a 2D  array to hold the rewards for each state.
// The array contains 11 rows and 11 columns (to match the shape of the environment), and each value is initialized to -100.

        // this.rewards = Array.from({
        //     length: this.environment_size
        // }, (v, k) => [...Array.from({
        //     length: this.environment_size
        // }, (v, k) => -100)]);


        this.printMaze(this.rewards);

        // Train the Agent using Q-Learning Algorithm

        // define training parameters
        let epsilon = 0.9 // The percentage of time when we should take the best action (instead of a random action)
        let discountFactor = 0.9 // Discount factor for future rewards
        let learningRate = 0.9 // The rate at which the agent should learn

        // Run through 1000 training episodes

        for (let i = 0; i<= 1000; i++) {
            // get the starting location for this episode
            let [rowIndex, columnIndex] = this.getStartingLocation()
            // continue taking actions (i.e., moving) until we reach a terminal state
            // ??? (i.e., until we reach the item packaging area or crash into an item storage location)

            //while(this.isTerminalState(rowIndex, columnIndex)) {
            console.log('while 1');
            // Choose which action to take (i.e., where to move next)
            let actionIndex = this.getNextAction(rowIndex, columnIndex, epsilon);
            let [newRowIndex, newColumnIndex] = this.getNextLocation(rowIndex, columnIndex, actionIndex)

            // Perform the chosen action, and transition to the next state (i.e., move to the next location)

            let oldRowIndex = rowIndex;
            let oldColumnIndex = columnIndex;  //store the old row and column indexes

            rowIndex = newRowIndex;
            columnIndex = newColumnIndex;

            // Receive the reward for moving to the new state, and calculate the temporal difference

            let reward = this.rewards[rowIndex][columnIndex];
            let oldQValue = this.qValues[oldRowIndex][oldColumnIndex];
            let temporalDifference = 0;

            if(this.qValues[rowIndex][columnIndex] !== 0) {
                temporalDifference = reward + (discountFactor * Math.max(...this.qValues[rowIndex][columnIndex])) - oldQValue;
            }

            // update the Q-value for the previous state and action pair

            let newQValue = oldQValue + (learningRate * temporalDifference)
            this.qValues[oldRowIndex][oldColumnIndex] = newQValue;
            console.log('this.qValues    ', this.qValues);
            console.log('Training complete!');
            //}
            console.log(this.getShortestPath(0, 0).toString()) // starting at row 3, column 9
        }

        //display a few shortest paths
        console.log(this.getShortestPath(0, 0).toString()) // starting at row 3, column 9
        //console.log(this.getShortestPath(5, 0).toString()) // starting at row 5, column 0
        //console.log(this.getShortestPath(9, 5).toString()) // starting at row 9, column 5
    }
}

const q = new Q();
q.main();






