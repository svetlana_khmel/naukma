
const environment_size = 6;
// define training parameters
const epsilon = 0 // The percentage of time when we should take the best action (instead of a random action)
const discountFactor = 0.9 // Discount factor for future rewards
const learningRate = 0.9 // The rate at which the agent should learn
let reward;
// Старт
// Пусті клітіни
// Вогняні ями (при потраплянні у них гра закінчується)
// Зелені клітинки “відпочинку” які дають невеликий бонус  (менше ніж винагорода за вихід).
// Вихід

const actions = ['up', 'right', 'down', 'left'];

const startPosition = [0,1];
const endPosition = [0,1];

const fireHolsValue = -100;

const greenCellsValue = 10;


let qValues = Array.from({
    length: environment_size
}, (v, k) => [...Array.from({
    length: environment_size
}, (v, k) => [0,0,0,0])]);


let rewards = Array.from({
    length: environment_size
}, (v, k) => [...Array.from({
    length: environment_size
}, (v, k) => -1)]);


rewards[0][5] = fireHolsValue;
rewards[2][0] = fireHolsValue;
rewards[2][3] = fireHolsValue;
rewards[2][4] = fireHolsValue;
rewards[3][0] = fireHolsValue;
rewards[4][5] = fireHolsValue;

rewards[1][3] = greenCellsValue;
rewards[2][4] = greenCellsValue;
//rewards[3][5] = greenCellsValue;
//rewards[0][2] = greenCellsValue;
//rewards[4][3] = greenCellsValue;
rewards[4][5] = 100;


function getShortestPath1 (startRowIndex, startColumnIndex, qValues, rewards) {
    // return immediately if this is an invalid starting location
    //console.log('return immediately if this is an invalid starting location    ');
    if(isTerminalState(startRowIndex, startColumnIndex)) {
        console.log('isTerminalState    ');
        return [];
    } else {

        let currentRowIndex = startRowIndex;
        let currentColumnIndex = startColumnIndex;
        let shortestPath = [[startRowIndex, startColumnIndex]];


        // up right down left

        let index;
        let indeces = []
        for (let i = 0; i < environment_size; i++) {
            for (let j = 0; j < environment_size; j++) {
                index = getInxedMaxElementInArray(qValues[i][j]);
                indeces.unshift(index)






                //console.log('qValues>>>> >>>>>  ', qValues[i][j]);
                //console.log('qValues>>>>   ', getInxedMaxElementInArray(qValues[i][j]));
                //const [x, y] = getNextLocation(currentRowIndex, currentColumnIndex, getInxedMaxElementInArray(qValues[i][j]));
                //currentRowIndex += x;
                //currentColumnIndex += y
                //shortestPath.push([currentRowIndex, currentColumnIndex])

            }
            // console.log('>>>>> qValues[i][j] ', qValues[i][j]);
            // console.log('>>>>> index ', index);

            if(index === 0) { // up
                currentRowIndex -= 1;
            }

            if(index === 1) { // right
                currentColumnIndex += 1;
            }

            if(index === 2) { // down
                currentRowIndex += 1;
            }

            if(index === 3) { // left
                currentColumnIndex -= 1;
            }

            shortestPath.unshift([currentRowIndex, currentColumnIndex]);
        }
        console.log('indeces',  indeces);
        console.log(' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>   ',  shortestPath);
        return shortestPath;

       // while (rewards[currentRowIndex][currentColumnIndex] !== 100) {
        //
        //     qValues
        //
        // }
        // if this is a 'legal' starting location
        //console.log('if this is a \'legal\' starting location    ');
        // let currentRowIndex = startRowIndex;
        // let currentColumnIndex = startColumnIndex;
        // let shortestPath = [];

        // continue moving along the path until we reach the goal (i.e., higest reward 100 )
        //console.log('continue moving along the path until we reach the goal    ');
        // while (!isTerminalState(currentRowIndex, currentColumnIndex)) {
        //     // get the best action to take
        //     //console.log(' // get the best action to take    ');
        //     let actionIndex = getNextAction(currentRowIndex, currentColumnIndex, 1);
        //     // move to the next location on the path, and add the new location to the list
        //
        //     currentRowIndex = getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[0];
        //     currentColumnIndex = getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[1];
        //
        //     shortestPath.push([currentRowIndex, currentColumnIndex]);
        //     return shortestPath;
      //  }
    }
}


// define an epsilon greedy algorithm that will choose which action to take next (i.e., where to move next)
function getNextAction (currentRowIndex, currentColumnIndex, epsilon) {
    //console.log('getNextAction    ');
    // if a randomly chosen value between 0 and 1 is less than epsilon,
    // then choose the most promising value from the Q-table for this state.
    // return Math.floor(Math.random() * 4);
    if (Math.floor(Math.random() * 10) * 0.1 < epsilon) {
        const indexMax = getInxedMaxElementInArray(qValues[currentRowIndex][currentColumnIndex]);
        console.log('indexMax   ', indexMax);
        console.log('indexMax  ----   ', qValues[currentRowIndex][currentColumnIndex]);
        return indexMax;
        //return Math.max(...qValues[currentRowIndex][currentColumnIndex]) || 0;
    } else {
        //console.log('choose a random action    ');
        //choose a random action
        //console.log('or random action:', Math.floor(Math.random() * 4));
        return Math.floor(Math.random() * 4); //?????????
    }
}

// define a function that will get the next location based on the chosen action
function getNextLocation(currentRowIndex, currentColumnIndex, actionIndex) {
    let newRowIndex = currentRowIndex;
    let newColumnIndex = currentColumnIndex;
    //console.log('define a function that will get the next location    ');
    if (actions[actionIndex] === 'up' && currentRowIndex > 0) {
        newRowIndex -= 1;
        //console.log('newRowIndex    ', newRowIndex);
    } else if (actions[actionIndex] === 'right' && currentColumnIndex < environment_size - 1) {
        newColumnIndex +=1;
        //console.log('newColumnIndex    ', newColumnIndex);
    } else if (actions[actionIndex] === 'down' && currentRowIndex < environment_size - 1) {
        newRowIndex += 1;
        //console.log('newRowIndex    ', newRowIndex);
    } else if (actions[actionIndex] === 'left' && currentColumnIndex > 0) {
        newColumnIndex -= 1;
        //console.log('newColumnIndex    ', newColumnIndex);
    }

    return [newRowIndex, newColumnIndex];
}

// Define a function that will get the shortest path between any location within the maze that
// the robot is allowed to travel and the item packaging location.

function getShortestPath (startRowIndex, startColumnIndex) {
    // return immediately if this is an invalid starting location
    //console.log('return immediately if this is an invalid starting location    ');
    if(isTerminalState(startRowIndex, startColumnIndex)) {
        console.log('isTerminalState    ');
        return [];
    } else {
        // if this is a 'legal' starting location
        //console.log('if this is a \'legal\' starting location    ');
        let currentRowIndex = startRowIndex;
        let currentColumnIndex = startColumnIndex;
        let shortestPath = [];
        shortestPath.push([currentRowIndex, currentColumnIndex]);

        // continue moving along the path until we reach the goal (i.e., higest reward 100 )
        //console.log('continue moving along the path until we reach the goal    ');
        let iterations = 0;
        while (!isTerminalState(currentRowIndex, currentColumnIndex)) {

            // get the best action to take
            //console.log(' // get the best action to take    ');
            let actionIndex = getNextAction(currentRowIndex, currentColumnIndex, 1);
            // move to the next location on the path, and add the new location to the list

            currentRowIndex = getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[0];
            currentColumnIndex = getNextLocation(currentRowIndex, currentColumnIndex, actionIndex)[1];

            shortestPath.push([currentRowIndex, currentColumnIndex]);


            return shortestPath;
            console.log('shortestPath  ', shortestPath);
        }


        console.log('reward  ', rewards[currentRowIndex, currentColumnIndex]);
        //
    }
}

function chooseMaxValueinArray(array, currentRowIndex, currentColumnIndex) {
    //console.log(qValues[currentRowIndex][currentColumnIndex])
    const  max = Math.max(...qValues[currentRowIndex][currentColumnIndex]) || 0
    return max;
}

function getInxedMaxElementInArray(array) {
    return array.indexOf(Math.max(...array));
}

// define a function that determines if the specified location is a terminal state
function isTerminalState (currentRowIndex, currentColumnIndex) {
    if (rewards[currentRowIndex][currentColumnIndex] === -1 || rewards[currentRowIndex][currentColumnIndex] === 10 ) {
        return false;
    } else {
        return true;
    }
}

// define a function that will choose a random, non-terminal starting location
function getStartingLocation () {
    //console.log('getStartingLocation    ');
    // get a random row and column index
    let currentRowIndex = Math.floor(Math.random() * environment_size);
    let currentColumnIndex = Math.floor(Math.random() * environment_size);
    // continue choosing random row and column indexes until a non-terminal state is identified
    // (i.e., until the chosen state is a 'white square').

    while(isTerminalState(currentRowIndex, currentColumnIndex)) {
        //console.log('не можемо ставити');
        currentRowIndex = Math.floor(Math.random() * environment_size);
        currentColumnIndex = Math.floor(Math.random() * environment_size);
    }
    return [currentRowIndex, currentColumnIndex];

    // continue choosing random row and column indexes until a non-terminal state is identified
    //(i.e., until the chosen state is a 'white square')
}

function chooseRandomAction() {
    return Math.random() < 0.5 ? 0 : 1;
}

// Run through 1000 training episodes

for (let i = 0; i<= 1000; i++) {
    // get the starting location for this episode
    let [rowIndex, columnIndex] = getStartingLocation();

    // continue taking actions (i.e., moving) until we reach a terminal state
    // (i.e., until we reach the item packaging area or crash into an item storage location)
    let iterations = 0;
    while(!isTerminalState(rowIndex, columnIndex)) {
        // console.log('iterations ... ', iterations);
        // console.log('rowIndex ... ', rowIndex);
        // console.log('columnIndex ... ', columnIndex);
        iterations ++;
        // Choose which action to take (i.e., where to move next)
        let actionIndex = getNextAction(rowIndex, columnIndex, epsilon);

        // Perform the chosen action, and transition to the next state (i.e., move to the next location)

        let oldRowIndex = rowIndex;
        let oldColumnIndex = columnIndex;  // store the old row and column indexes

        rowIndex = getNextLocation(rowIndex, columnIndex, actionIndex)[0];
        columnIndex = getNextLocation(rowIndex, columnIndex, actionIndex)[1];

        // Receive the reward for moving to the new state, and calculate the temporal difference
        // if (!rewards[rowIndex]) {
        //     continue;
        // }
        reward = rewards[rowIndex][columnIndex];
        let oldQValue = qValues[oldRowIndex][oldColumnIndex][actionIndex];
        const max = chooseMaxValueinArray(qValues, rowIndex, columnIndex);
        //console.log('qValues[rowIndex][columnIndex] .... ', qValues[rowIndex][columnIndex]);
        let temporalDifference = reward + (discountFactor * max) - oldQValue;

        // Update the Q-value for the previous state and action pair

       // let newQValue = oldQValue + Math.round((learningRate * temporalDifference) * 100) / 100
        let newQValue = oldQValue + (learningRate * temporalDifference)

        qValues[oldRowIndex][oldColumnIndex][actionIndex] = newQValue;
        console.log('Training complete!', );
    }
    console.log('Training complete!', );
    console.log('qValues    ', qValues);
    console.log('reward    ', rewards);
    console.log('Training complete! ' , getShortestPath(0, 0)) // starting at row 0, column 0
    //console.log('Training complete! ' , getShortestPath1(0, 0, qValues, rewards)) // starting at row 0, column 0
    //console.log('Training complete! ' , getShortestPath1(0, 0, qValues).toString())
}






