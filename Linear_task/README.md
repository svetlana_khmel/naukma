
See screenshot:
https://res.cloudinary.com/dmt6v2tzo/image/upload/v1632986856/Screenshot_2021-09-30_at_10.27.03_rpe1od.png
----------------------
Задача лінійного програмування
----------------------
Продукт 1 (Хліб) Ціна за 1 15 грн
Продукт 1 (Макарони) Ціна за 1 50 грн

Ресурс1: (c1) 1000 (Борошно)

Ресурс 2: (с2) 500  (Цукор)

Ресурс 3: (с3) 300 (Олiя)

1 массив = ціни за 1 продукції
2 массив = ресурси (обмеження)


-----------------------------------
Скільні обмещення ресурсу на 1 продукції хліби та борошна:
Продукт 1 (Хліб) (Витрачаємо):

Борошно: 10 (a1)

Цукор 5 (a2)

Олiя 3 (a3)

-----------------------------------
Продукт 2 (Макарони) (Витрачаємо):

Борошно: 15 (b1)

Цукор: 5 (b2)

Олiя: 2 (b3)

-----------------------------------
Який максимальний прибуток ми можемо отримати?

Лiнія рівня


Рівняння:
a1x + b1y <= c1;
a5x + b5y <= c2;
a3x + b2y <= c3;

x >= 0;
y >= 0;

Підставляємо значення, приводимо до канонічного виду:

10x + 15y - 1000 = 0
5x + 5y - 500 = 0
3x + 2y - 300 = 0
---

---

## Resources

Watch:

1. Just for information: https://www.youtube.com/watch?v=hlSSfJKrNKw.
2. https://www.google.com/search?q=simplex+method+visualization&rlz=1C5CHFA_enUA956UA956&oq=s&aqs=chrome.1.69i60j69i59l2j69i60l3j69i65l2.5891j0j7&sourceid=chrome&ie=UTF-8#scso=_Z85QYZPwGoSWrwSb5I6gAg27:0
3. Simplex method for solving the linear programming problem: https://www.youtube.com/watch?v=E-SuGjUjJ3Y

## Libs

1. JSXGraph https://jsxgraph.uni-bayreuth.de/
