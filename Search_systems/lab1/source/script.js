const path = require('path');
const fs = require('fs');

const directoryPath = path.join(__dirname, '../txt');
let slovnik = {};
let dict = [];

fs.readdir(directoryPath, function (err, files) {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }

    for (let index = 0; index < files.length; index++ ) {

        // 1) Считать содержимое файла
        fs.readFile(`../txt/${files[index]}`, 'utf8' , (err, data) => {
            if (err) {
              console.error(err)
              return
            }
            // 2) Добавить в массив только слова    
            dict = [...dict, ...data.match(/\b(\w+\D\S)\b/g)]

            if (index === files.length-1) {
            // 3) Рахуємо слова    
                for (let i = 0; i< dict.length; i++) {
                
                    if(slovnik[dict[i].trim()])  {
                        slovnik[dict[i]] = slovnik[dict[i]]+1;    
                    }
                    if(!slovnik[dict[i].trim()]) {
                        slovnik[dict[i]] = 1;
                    }
                }
                console.error('dict ', dict);
                console.log('slovnik', slovnik)
                console.log('Number of words: ', Object.keys(slovnik).length);    

                // 3) Записуємо в файли      
                fs.writeFile('slovnik.txt', Object.keys(slovnik).toString(), function (err) {
                    if (err) return console.log(err);
                    console.log('Saved .txt!');
                 });

                 fs.writeFile('slovnik.json', JSON.stringify(slovnik), function (err) {
                    if (err) return console.log(err);
                    console.log('Saved .json!');
                 });

                 try {
                    const stats = fs.statSync('slovnik.txt');
                    const fileSizeInBytes = stats["size"]
                    console.log('Size:', fileSizeInBytes/1000,'kb');
                } catch (err) {
                    console.log(err);
                }
            }
        })
    }
});

`
>> To execute file run in the console:

    node script.js
` 











