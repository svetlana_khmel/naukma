// Check if the pair of symbol contains Bracket
// If true , return the symbol
// Set isOpenBracket = 'on'

// Check if the pair of symbol contains Bracket
// If true , return the symbol
// Set isOpenBracket = 'of'

// Find the the single line comment (//)
// Set status singleLineComment = 'on'
// Don't return symbol

// Find the end of the string
// Set status singleLineComment = 'off'

// Find the the multi line comment sign  (/*)
// Set status isMultiLineOpenComment = 'on'
// Don't return symbol

// Find the the multi line comment sign  (/*)
// Set status isMultiLineOpenComment = 'off'
// Don't return symbol

const fs = require('fs'),
    path = require('path');


const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let singleLineComment = false;
let isMultiLineComment = false;
let isOpenBracket = false;

const isTheEndOfLine = (symbol) => {
    const match = /\r|\n/.test(symbol);
    return match;
}

const isBracketSign = (symbol) => {
    const match = /\"/.test(symbol);
    return match;
}

const isSingleLineCommentSign = (symbols) => {
    const match = /\/\//.test(symbols);
    return match;
}

const isMultiLineOpenCommentSign = (symbols) => {
    const match = /\/\*/.test(symbols);
    return match;
}

const isMultiLineCloseCommentSign = (symbols) => {
    const match = /\*\//.test(symbols);
    return match;
}

const stringExample = `/// Godzilla 1
Mozilla000 // GodzillaVV 2 XXXXGodzillaGodzillaGodzillad
"// Godzilla"
"/* Godzilla */"
/* Godzilla */
MozillaMozillaMozillaMozillaMozillaMozilla Mozilla`;

function convertToArray(str) {
    const inputArray = [...str];
    const output = inputArray.map((symbol, i) => {
        const symbolsToCheck = [symbol, inputArray[i+1]].join('');
        const symbolsToCheckPrevious = [inputArray[i-2], inputArray[i-1]].join('');

        if(isBracketSign(symbol)) {
            isOpenBracket = !isOpenBracket;
        }

        if (isSingleLineCommentSign(symbolsToCheck)) {
            singleLineComment = true;
            console.log('singleLineComment   ', singleLineComment)
        }

        if (singleLineComment && isTheEndOfLine(symbol)) {
            singleLineComment = false;
            console.log('singleLineComment   ', singleLineComment)
        }

        if (isMultiLineOpenCommentSign(symbolsToCheck)) {
            isMultiLineComment = true;
            console.log('isMultiLineComment   ', isMultiLineComment);
        }

        if (isMultiLineCloseCommentSign(symbolsToCheckPrevious)) {
            isMultiLineComment = false;
        }

        if(isOpenBracket && singleLineComment || isOpenBracket && isMultiLineComment) {
            return symbol;
        }

        if (singleLineComment ||
            isMultiLineComment
        ) {
            return;
        }

        return symbol;
    });

    return output.join('');
}

rl.question('Enter the name of the input file: ', (answer) => {
    console.log(`Thank you : ${answer}`);
    const filePath = path.join(__dirname, answer);

    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        if (!err) {
            const result = convertToArray(data);

            console.log('RESULT:::   ', result);

            fs.writeFile(`new-${answer}`, result, function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("The file was saved!");
            });
        } else {
            console.log(err);
        }
    });

    rl.close();
});
