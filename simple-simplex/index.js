const SimpleSimplex = require('simple-simplex');

// a+b+c+d=35
// e+f+g+h=50
// j+k+l+m=15

// a+e+j=30
// b+f+k=10
// c+g+l=20
// d+h+m=40
//a+3b +2c+4d+2e+1f+4g+3h+3j+5k+6l+1m+1l
// initialize a solver
const solver = new SimpleSimplex({
    objective: {
        a: 1,
        b: 3,
        c: 2,
        d: 4,
        e: 2,
        f: 1,
        g: 4,
        h: 3,
        j: 9,
        k: 5,
        l: 6,
        m: 1,
    },
    constraints: [
        {
            namedVector: { a: 1, b: 1, c: 1, d: 1, e: 0, f: 0, g: 0, h: 0, j: 0, k: 0, l: 0, m: 0 },
            constraint: '<=',
            constant: 35,
        },
        {
            namedVector: { a: 0, b: 0, c: 0, d: 0, e: 1, f: 1, g: 1, h: 1, j: 0, k: 0, l: 0, m: 0 },
            constraint: '<=',
            constant: 50,
        },
        {
            namedVector: { a: 0, b: 0, c: 0, d: 0, e: 0, f: 0, g: 0, h: 0, j: 1, k: 1, l: 1, m: 1 },
            constraint: '<=',
            constant: 15,
        },
        {
            namedVector: { a: 1, b: 0, c: 0, d: 0, e: 1, f: 0, g: 0, h: 0, j: 1, k: 0, l: 0, m: 0 },
            constraint: '<=',
            constant: 30,
        },
        {
            namedVector: { a: 0, b: 1, c: 0, d: 0, e: 0, f: 1, g: 0, h: 0, j: 0, k: 1, l: 0, m: 0 },
            constraint: '<=',
            constant: 10,
        },
        {
            namedVector: { a: 0, b: 0, c: 1, d: 0, e: 0, f: 0, g: 1, h: 0, j: 0, k: 0, l: 1, m: 0 },
            constraint: '<=',
            constant: 20,
        },
        {
            namedVector: { a: 0, b: 0, c: 0, d: 1, e: 0, f: 0, g: 0, h: 1, j: 0, k: 0, l: 0, m: 1 },
            constraint: '<=',
            constant: 40,
        },
    ],
    optimizationType: 'min',
});

// call the solve method with a method name
const result = solver.solve({
    methodName: 'simplex',
});

// see the solution and meta data
console.log({
    solution: result.solution,
    isOptimal: result.details.isOptimal,
});
