IT IS BETTER TO USE THIS LIBRARY
---

## The task is:

Make calculation, using simplex method.

1. Check if results matched with a graphic solution in the 'Linear_task' .

---

## Libs:

Simplex Method calculation:

Let's solve the following problem:

Maximize 70a + 210b + 140c, subject to the following constraints:

`
a + b + c <= 100

5a + 4b + 4c <= 480

40a + 20b + 30c <= 3200

`
In order to solve this problem, we need to convert it to a language that our algorithm can understand.

Once the problem is converted, we initialize a solver instance.

We then call solver.solve with the solution method we want. There are many different methods that can solve the Linear Programming problem. We will update the readme once we implement new methods. For now, use the following way to call the solver.solve function.

Finally, once the solver returns a result, we can see a solution and some meta data, such as isOptimal and allTableaus, as well as solution.

`
const SimpleSimplex = require('simple-simplex');

// initialize a solver
const solver = new SimpleSimplex({
objective: {
a: 70,
b: 210,
c: 140,
},
constraints: [
{
namedVector: { a: 1, b: 1, c: 1 },
constraint: '<=',
constant: 100,
},
{
namedVector: { a: 5, b: 4, c: 4 },
constraint: '<=',
constant: 480,
},
{
namedVector: { a: 40, b: 20, c: 30 },
constraint: '<=',
constant: 3200,
},
],
optimizationType: 'max',
});

// call the solve method with a method name
const result = solver.solve({
methodName: 'simplex',
});

// see the solution and meta data
console.log({
solution: result.solution,
isOptimal: result.details.isOptimal,
});
`
