https://matworld.ru/calculator/simplex-method-online.php
---

---

## Resources

Watch:

1. Just for information: https://www.youtube.com/watch?v=hlSSfJKrNKw.
2. https://www.google.com/search?q=simplex+method+visualization&rlz=1C5CHFA_enUA956UA956&oq=s&aqs=chrome.1.69i60j69i59l2j69i60l3j69i65l2.5891j0j7&sourceid=chrome&ie=UTF-8#scso=_Z85QYZPwGoSWrwSb5I6gAg27:0
3. Simplex method for solving the linear programming problem: https://www.youtube.com/watch?v=E-SuGjUjJ3Y

## Libs

1. JSXGraph https://jsxgraph.uni-bayreuth.de/
