Цепи Маркова
---

## The task is:

1. Построить матрицу (вектор) первичных вероятностей
2. Построить матрицу переходов с 3-4 состояниями
3. Построить граф системы
4. Выяснить  система эргодичная или нет
5. Проверить есть ли граничные значения (и сколько итераций)
6. Решить систему линейных уравнений для нахождения П
7. Метод простых итераций

- P - однорідна матріця переходів (перехідніх вірогідностей)
- а - первинні вірогідності

- Елемент матриці переходів отрімуємо множеням



## Тезисы:
Эргодична - є не 0-ва ймовірність повернутіся до мінулого стану за скінченну кільність кроків
Якшо сістема Эргодична - то є граничний розподіл (зупінка ітерацій)

SS - сі - стан системи - 
абсолютний метод розподілу ймовірностей того, що сістема перебуває в певному стані

## Материалы:
Таха_Введение_в_исследование_операций


