
---

## The task is:

Make calculation, using simplex method.

1. Check if results matched with a graphic solution in the 'Linear_task' .

---

## Libs:

Simplex Method calculation:

https://github.com/LarryBattle/YASMIJ.js

Use these steps:
1. Go to /demo.
2. Objective (product prices) 1.15x1 + 1.50x2.
3. Constraint 1: 3x1 + 2x2 <= 200
4. Constraint 2: 5x1 + 5x2 <= 350
5. Constraint 3: 10x1 + 15x2 <= 900
